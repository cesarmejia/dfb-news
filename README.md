# Django for Beginners - Newspaper App

- Deployed app [here](https://dfb-newspaper-cm.herokuapp.com/)

## Chapter 08: Custom User Model

- **Important** DO NOT LAUNCH MIGRATIONS until `CustomUser` flow complete

- **Best Practice** always use a custom user model for new projects

- Django was originally built for editors and journalist at the `Lawrence Journal-World`

- Custom user app sometimes called `accounts` instead of `users`

- `null=True` ... **database-related** setting ... allows NULL in field record

- `blank=True` ... **validation-related** seting ... allows form field to be empty

- **Best Practice** for string-based fields; only `blank=True`; Empty string indicates absence of value not NULL.

- _To add our custom `age` field we simply tack it on at the end and it will display automatically on our future sign up page. Pretty slick, no?_

- _The concept of fields on a form can be confusing at first so let's take a moment to explore it further._

- _We will extend the existing `UserAdmin` class to use our new `CustomUser` model_

- **TODO: Breakdown Django Admin panel into React components**

- _In the next chapter we will configure and customize sign up, log in, and log out pages_

- Prefer `CustomUser` to inherit from `AbstractUser` instead of `AbstractBaseUser` -> less hassle

- `forms.py` -> Extends `UserCreationForm` -> `CustomUserCreationForm` and `UserChangeForm` -> `CustomUserChangeForm` since `User` extended to `CustomUser`

  - Uses `class Meta` to extend fields; this subclass inherits from appropriate form
  - ??? Why not `fields = UserChangeForm.Meta.fields + ('age',)` below:

  ```python
  class CustomUserChangeForm(UserChangeForm):

    # why not? ... fields = UserChangeForm.Meta.fields + ('age',); explicitly add custom field
    class Meta:
      model = CustomUser
      fields = UserChangeForm.Meta.fields
  ```

- `UserCreationForm` default values are `username`, `email`, and `password`

- In `admin.py`, we need to extend `django.contrib.auth.admin.UserAdmin` -> `CustomUserAdmin`

  - Need to set: `model`, `add_form`, `form` - custom creation and change form, respectively

- As before, register models: `admin.site.register(CustomUser, CustomUserAdmin)`

- `list_display` alters fields displayed in Django admin panel; property of CustomUserAdmin; list of fields

## Chapter 09: User Authentication

- Add `templates` dir to root of project to avoid app template nesting pattern

  - Requires edit to `settings.py`, `TEMPLATES`: `'DIRS': [os.path.join(BASE_DIR, 'templates')],`

- To redirect after login/logout, set `LOGIN_REDIRECT_URL` and `LOGOUT_REDIRECT_URL` in `settings.py`

- **NOTE** Django expects `login.html` template to be within `registration` dir (e.g. `templates/registration/login.html`)

- When using a custom user, `django.contrib.auth.urls` handles login/logout urls, views, and templates

  - e.g. `path('users/', include('django.contrib.auth.urls'))` .. similar to how `path('admin/', admin.site.urls)` handles admin urls, views, and templates

- `SignUpView` inherits from `django.views.generic.CreateView`

  - Requires `form_class`, `success_url`, and `template_name` to be declared

  ```python
  class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
  ```

## Chapter 10: Bootstrap & HomePageView/SignUpView Test Suite

- Need to test `home` and `signup` pages; `login` and `logout` are handled by Django core features

- Testing Pattern. For each static page, tutorial test 3 things:

  1. Test `self.client.get` to hardcoded url returns correct status code
  2. Test `self.client.get` to `named url` return correct status code
  3. Test `self.client.get` uses correct named template (i.e. `self.assertTemplateUsed`)

- SignUp Test. Reference user model with `django.contrib.auth.get_user_model` method
- Uses auth.User objects manager `create_user` method to create and save a user to test db

  - e.g. `get_user_model().objects.create_user(self.username, self.email)` ... _weird that password isn't required_
  - Test further check object count equal 1, and username, email attributes set correctly

- Since we are using `React` instead of templates to generate FE pages, not much interesting in this chapter
  - Used `django-crispy-forms` to change default form styling
  - Requires adding `crispy-forms` to `INSTALLED_APPS` in `settings.py`
    - Also need to set `CRISPY_TEMPLATE_PACK = 'bootstrap4'` in `settings.py`
    - In `signup` template, `{% load crispy_forms_tags %}` and replace `{{ form.as_p }}` with `{{ form|crispy }}`

## Chapter 11: Password Change and Reset

- Enable password change and password reset views

- Template heavy chapter; Mainly spent time customizing `password_change` and `password_reset` templates (flow below):

  - `password_change_form.html`
  - `password_change_done.html`
  - `password_reset_form.html`
  - `password_reset_done.html`
  - `password_reset_confirm.html`
  - `password_reset_complete.html`

- For development, in `settings.py`, set `EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'`
  - outputs password reset email to console.

## Chapter 12: Email

- _At this point you may be feeling a little overwhelmed by all the user authentication configuration we've done up to this point. That's normal._

- _As you become more and more experienced in web development, the wisdom of Django's approach will ring true._

- In chapter 11, password_reset emails were being sent to the console, let's fix that.

- Will implement SendGrid to enable SMTP sending of password reset emails to users

- **Pretty sure I should be hiding my keys in environmental variables.**

- In `settings.py`, change/add:

  - `EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

  ```python
  EMAIL_HOST = 'smtp.sendgrid.net'
  EMAIL_HOST_USER = 'apikey'
  EMAIL_HOST_PASSWORD = '[SEND_GRID_API_KEY]'
  EMAIL_PORT = 587
  EMAIL_USE_TLS = True
  ```

- `registration/password_reset_email.html` change if you wish to customize template

  - ??? How to change `site_name` referenced in above template? `reset_link` block; `token`

- `registration/password_reset_subject.txt` to change email subject line

- ??? I would like to customize the variables available to the password reset email template

## Chapter 13: Newspaper App (not just auth and password 🙃)

- PSD:
  - A journalist will be able to post articles.
  - Only the author (a journalist identified by `pk` (Many-to-One FK on Article model link to `pk` on Journalist model)) of an article will be able to edit or delete an article
  - A user will be able to post comments to an article (Many-to-One FK on Comments link to `pk` on Article model)

## Chapter 14: Permissions and Authorization

- _Authorization_ restricts access; _authentication_ enables a user sign up and log in flow.

- We'll limit web page access to logged-in users.

- _The more you use and customize built-in views, the more comfortable you will become making customizations like this._

- Add `form_valid` method to `ArticleCreateView`

```python
def form_valid(self, form):
    form.instance.author = self.request.user
    return super().form_valid(form)
```

- **Mixins** a special kind of multiple inheritance that Django uses to avoid duplicate code and skill allows customization.

- `LoginRequiredMixin` from `django.contrib.auth.mixins` can be used to redirect not logged-in users to the login page on protected views

- `UserPassesTestMixin` from `django.contrib.auth.mixins` can be used authorize users via `test_func`

- ??? Improvement, only show `Edit` and `Delete` to properly authorized users

## Chapter 15: Comments

- Additional PSD:

  - Users will be able to add comments to any user article.

- **Best Practice** Keep each migration as small and contained as possible by designating which app migrations should run on

- _Understanding queries takes some time so don't be concerned if the idea of reverse relationships is confusing_

- **Further Work** Add Article Creation Form on `articles/` as well as ability to add `comments` directly on `article_detail.html` template

  - Add `age dropdown` to SignUp form; restrict sign up to >= 13 years old
  - Switch to `postgreSQL` for deployment
  - Use environmental variables (`activate`) to hide secret keys
  - Add `discounts` to users >= 65 years old (Django for Professionals - `payments` app)
  - Turn `Newspaper` app to a `Todo List` app and allow for user registration

- _Most of web development follows the same patterns and by using a web framework like Django 99% of what we want in terms of functionality is either already included or only a small customization of an existing feature away_

## Conclusion

- Finished Django for Beginners (TODO: go back and add more notes)

- _Web development is a very deep field and there's always something new to learn. When you're starting out I believe the best approach is to build as many small projects as possible and incrementally add complexity and research new things._

- `Django for Professional` tackles _production-ready_ challenges:

  - `Deploying with Docker`
  - `Provisioning PostgreSQL` database
  - `Handling payments`
  - `Environment variables`
  - `Advanced user registration`
  - `Security`
  - `Performance`

- `Django for APIs`

  - _Creating a full-stack website is quite the challenge for a single developer._
  - _However if you talk to professional Django developers who work at either small startups or large corporations, chances are the majority of their time is spent solely on the back-end creating Django-based web APIs. 🤷‍♀️_

- Open-Source Resources (Curated by the author of this book trilogy, William Vincent):

  - [Awesome-Django](https://github.com/wsvincent/awesome-django)
  - [DjangoX](https://github.com/wsvincent/djangox)
    - A framework for launching new Django projects quickly. Comes with a custom user model, email/password authentication, options for social authentication via Google/Facebook/Twitter/etc, and static assets.
  - [DRFX](https://github.com/wsvincent/drfx)
    - A framework for launching new Django Rest Framework projects quickly.

- Don't fear using [Django documentation](https://www.djangoproject.com/) and [source code](https://github.com/django/django) as a resource as Django-fu further develops

- [Classy Class-Based Views](https://ccbv.co.uk/)

  - Detailed descriptions, with full methods and attributes, for each of Django's class-based generic views.

- When time allows:

  - [DjangoCon conference talks](https://www.youtube.com/playlist?list=PL2NFhrDSOxgXmA215-fo02djziShwLa6T)
  - [Two Scoops of Django](https://www.amazon.com/gp/product/0692915729/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=0692915729&linkId=42ac7eb52adc397a78080f12f7f839b1)

    - Best Practices Bible for Django

  - Python Resources:

    - Beginners:

      - Eric Matthes's [Python Crash Course](https://www.amazon.com/gp/product/1593276036/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1593276036&linkId=1f0cfaa42d12dcf4de967c31ccaad831)
      - Al Sweigart's [Automate the Boring Stuff](https://www.amazon.com/gp/product/1593275994/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1593275994&linkId=10e3efd8d2bc01606996d30eab8bbd8e)

    - Intermediate to Advance:
      - [Fluent Python](https://www.amazon.com/gp/product/1491946008/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1491946008&linkId=9d73ed7c4523ffd8f1c2380672de0c3b), [Effective Python](https://www.amazon.com/gp/product/0134034287/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=0134034287&linkId=6c823c0137898e6e7fabfc245f387eaa), and [Python Tricks](https://www.amazon.com/gp/product/1775093301/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=wsvincent-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1775093301&linkId=909a50c337bebad60a277b334c4bd1a2)

**Project Complete!!!**
